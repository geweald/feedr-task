import React from 'react';
import { shallow } from 'enzyme';

import { Circle } from './Circle';

describe('<Circle />', () => {
  it('renders', () => {
    const div = shallow(<Circle>test</Circle>);
    expect(div).toMatchSnapshot();
  });
});
