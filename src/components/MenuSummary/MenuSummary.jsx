import React from 'react';
import styled from 'styled-components';

import { Circle } from '../Circle';

export const MenuSummary = ({ itemsCount, dietaries }) => {
  return (
    <Wrapper>
      <div className="container">
        <div className="row">
          <SummaryLeft className="col-6">
            <span>{itemsCount} items</span>
          </SummaryLeft>
          <SummaryRight className="col-6">
            {dietaries.map(dietary => (
              <React.Fragment key={dietary.name}>
                {dietary.count}x<Circle>{dietary.name}</Circle>
              </React.Fragment>
            ))}
          </SummaryRight>
        </div>
      </div>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  background: #f8f8f8;
  padding: 30px 0;
`;

const SummaryLeft = styled.div`
  font-weight: bold;
`;

const SummaryRight = styled.div`
  text-align: right;
`;
