import React from 'react';
import { shallow } from 'enzyme';

import { MenuSummary } from './MenuSummary';

describe('<MenuSummary />', () => {
  it('renders', () => {
    const div = shallow(
      <MenuSummary itemCount={10} dietaries={[{ count: 7, name: 'dietary 1' }, { count: 10, name: 'dietary 2' }]} />
    );
    expect(div).toMatchSnapshot();
  });
});
