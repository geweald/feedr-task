import React from 'react';
import { shallow } from 'enzyme';

import { ItemRow } from './ItemRow';

describe('<ItemRow />', () => {
  it('renders', () => {
    const div = shallow(<ItemRow item={{ name: 'test name', dietaries: ['test dietary 1', 'dietary 2'] }} />);
    expect(div).toMatchSnapshot();
  });
});
