import React from 'react';
import styled from 'styled-components';

import { Circle } from '../Circle';

export const ItemRow = ({ item: { name, dietaries }, selected }) => {
  return (
    <Container selected={selected}>
      <h2>{name}</h2>
      <p>
        {dietaries.map(dietary => (
          <Circle key={dietary}>{dietary}</Circle>
        ))}
      </p>
    </Container>
  );
};

const Container = styled.div`
  border-radius: 5px;
  background: ${props => (props.selected ? 'green' : '#fff')};
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
  padding: 10px;
  margin-bottom: 20px;
  position: relative;

  & h2 {
    font-size: 12pt;
  }

  & p {
    margin: 0;
  }
`;
