import styled from 'styled-components';

export const CircleBtn = styled.button`
  position: absolute;
  top: -13px;
  right: -13px;
  -webkit-appearance: none;
  border: 2px solid #000;
  background: #fff;
  font-weight: bold;
  width: 26px;
  height: 26px;
  line-height: 0px;
  border-radius: 100%;
  padding: 0;
  cursor: pointer;

  &:focus {
    outline: none;
  }
`;
