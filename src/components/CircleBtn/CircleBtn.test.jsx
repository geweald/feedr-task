import React from 'react';
import { shallow } from 'enzyme';

import { CircleBtn } from './CircleBtn';

describe('<CircleBtn />', () => {
  it('renders', () => {
    const div = shallow(<CircleBtn>test</CircleBtn>);
    expect(div).toMatchSnapshot();
  });
});
