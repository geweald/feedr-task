import React from 'react';
import { Main } from './pages/Main';
import { itemsMock } from './mocks/items';

export const App = () => {
  return <Main items={itemsMock} />;
};
