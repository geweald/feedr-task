import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';

import { Main } from './Main';
import { itemsMock } from '../../mocks/items';
import { CircleBtn } from '../../components/CircleBtn';

describe('<Main /> Page', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Main items={itemsMock} />, div);
  });

  describe('logic', () => {
    let wrapper;

    beforeEach(() => {
      wrapper = shallow(<Main items={itemsMock} />);
    });

    it('should select item on click sidebar', () => {
      wrapper
        .find('.col-4')
        .find('li')
        .first()
        .simulate('click');

      const menuPreviewItems = wrapper.find('.col-8').find('li');

      expect(menuPreviewItems.length).toEqual(1);
    });

    it('should not select item on click if already selected', () => {
      const listItem = wrapper.find('li').first();
      listItem.simulate('click');
      listItem.simulate('click');

      const menuPreviewItems = wrapper.find('.col-8').find('li');

      expect(menuPreviewItems.length).toEqual(1);
    });

    it('should remove selected item on remove button click', () => {
      wrapper
        .find('li')
        .first()
        .simulate('click');
      let menuPreviewItems = wrapper.find('.col-8').find('li');
      expect(menuPreviewItems.length).toEqual(1);

      wrapper.find(CircleBtn).simulate('click');
      menuPreviewItems = wrapper.find('.col-8').find('li');
      expect(menuPreviewItems.length).toEqual(0);
    });
  });
});
