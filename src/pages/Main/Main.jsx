import React from 'react';
import styled from 'styled-components';

import { ItemRow } from '../../components/ItemRow';
import { MenuSummary } from '../../components/MenuSummary';
import { CircleBtn } from '../../components/CircleBtn';

export const Main = ({ items }) => {
  const [selectedItems, setSelectedItems] = React.useState([]);

  const selectedDietaries = React.useMemo(() => {
    const dietariesMap = {};
    selectedItems.forEach(({ dietaries }) => {
      dietaries.forEach(dietary => {
        dietariesMap[dietary] = (dietariesMap[dietary] || 0) + 1;
      });
    });
    return Object.entries(dietariesMap).map(([name, count]) => ({ name, count }));
  }, [selectedItems]);

  const removeItem = index => {
    const newItems = [...selectedItems];
    newItems.splice(index, 1);
    setSelectedItems(newItems);
  };

  const selectItem = item => {
    if (!selectedItems.includes(item)) {
      setSelectedItems([...selectedItems, item]);
    }
  };

  return (
    <Wrapper>
      <MenuSummary itemsCount={selectedItems.length} dietaries={selectedDietaries} />

      <MenuBuilder className="container">
        <div className="row">
          <div className="col-4">
            <ItemPicker>
              {items.map(item => (
                <li key={item.id} onClick={() => selectItem(item)}>
                  <ItemRow item={item} selected={selectedItems.includes(item)} />
                </li>
              ))}
            </ItemPicker>
          </div>

          <div className="col-8">
            <h2>Menu preview</h2>
            <MenuPreview>
              {selectedItems.map((item, index) => (
                <li key={item.id}>
                  <ItemRow item={item} />
                  <CircleBtn onClick={() => removeItem(index)}>X</CircleBtn>
                </li>
              ))}
            </MenuPreview>
          </div>
        </div>
      </MenuBuilder>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 50px 0;
  font-family: 'Nunito', sans-serif;
`;

const MenuBuilder = styled.div`
  margin: 50px 0;
`;

const MenuPreview = styled.ul`
  padding: 20px;
  list-style: none;
  margin: 0;

  & li {
    position: relative;
  }
`;

const ItemPicker = styled.ul`
  background: #f8f8f8;
  padding: 20px;
  list-style: none;
  margin: 0;
  height: 500px;
  overflow: scroll;
  border-radius: 5px;

  & li:hover {
    cursor: pointer;
  }
`;
